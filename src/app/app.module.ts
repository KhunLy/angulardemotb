import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NavComponent } from './nav/nav.component';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { MarcelInterceptor } from './demo/components/demo13/marcel.interceptor';
import { LoaderComponent } from './exercice/components/loader/loader.component';
import { LoadingInterceptor } from './exercice/components/exo4/interceptors/loading.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    NavComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide : HTTP_INTERCEPTORS, useClass : MarcelInterceptor, multi : true},
    { provide : HTTP_INTERCEPTORS, useClass : LoadingInterceptor, multi : true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
