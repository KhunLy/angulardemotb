import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ObservableService } from './observable.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './demo14.component.html',
  styleUrls: ['./demo14.component.scss']
})
export class Demo14Component implements OnInit, OnDestroy {

  destroyed$: Subject<any>;

  constructor(
    private obsService: ObservableService
  ) { 
    this.destroyed$ = new Subject<any>();
  }

  // cycle de vie à la destruction du component
  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  // cycle de vie appelé à l'initialisation
  ngOnInit(): void {
    this.obsService.obs$
    // jusqu'à ce que un autre obvervable soit déclenché
    .pipe(takeUntil(this.destroyed$))
    // abonnement à l'observable
    .subscribe(
      console.log, 
      error => { console.log("error") }, 
      () => { console.log('finish') }
    );
  }

}
