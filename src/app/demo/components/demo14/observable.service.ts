import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObservableService {

  obs$: Observable<number>
  obs2$: Subject<boolean>;
  obs3$: BehaviorSubject<boolean>;



  constructor() { 
    this.obs$ = new Observable<number>(o => {
      let val = 0;
      setInterval(() => {
        o.next(val++);
      }, 1000)
    });

    
    //this.obs$.next();
    this.obs2$ = new Subject<boolean>();
    this.obs2$.next(true);
    this.obs2$.next(false);

    this.obs3$ = new BehaviorSubject<boolean>(true);
    this.obs3$.next(false);
  }
}
