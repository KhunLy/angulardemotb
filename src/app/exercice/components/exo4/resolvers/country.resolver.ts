import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { CountryModel } from '../models/country.model';
import { CountryService } from '../services/country.service';

@Injectable({
  providedIn: 'root'
})
export class CountryResolver implements Resolve<CountryModel[]> {

  constructor(
    private countryService: CountryService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<CountryModel[]> {
    return this.countryService.getAll();
  }
}
