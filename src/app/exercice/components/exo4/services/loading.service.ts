import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  isLoading$: BehaviorSubject<boolean>;

  constructor() { 
    this.isLoading$ = new BehaviorSubject<boolean>(false);
  }
}
