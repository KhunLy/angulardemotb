import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryModel } from './models/country.model';
import { CountryService } from './services/country.service';

@Component({
  templateUrl: './exo4.component.html',
  styleUrls: ['./exo4.component.scss']
})
export class Exo4Component implements OnInit {

  countries!: CountryModel[];

  selectedCountryIso!: string;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    //this.countries = this.route.snapshot.data.countries;
    this.route.data.subscribe(data => this.countries = data.countries);
  }

}
